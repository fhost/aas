\documentclass[12pt,a4paper]{article}
\usepackage{color}
\usepackage{xcolor}
\usepackage[latin1]{inputenc}
\usepackage[left=2cm, right=2cm, bottom=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{eurosym}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{float}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{indentfirst}
\usepackage{subcaption}
\usepackage{caption}
\usepackage{array}
\usepackage{abstract}
\usepackage{multicol}
\usepackage{multirow}
\setlength\columnsep{20pt}

\bibliographystyle{alpha}

\lstset{
  morekeywords={for,if,else,return,do,then},
  basicstyle=\footnotesize\ttfamily,
  tabsize=2,%
  showspaces=false%,
  showstringspaces=false%, %
}

\usepackage{titlesec}
\titleformat{\section}[block]{\large\bfseries\scshape\centering}{\thesection.}{0.5em}{}
\renewcommand{\thesection}{\Roman{section}}

\newcommand{\makeabstract}[1]{\begin{abstract}\textit{#1}\end{abstract}\vspace{10mm}}


\title{\fontfamily{phv}\vspace{-20mm}\center\textsc{\textbf{Finding Approximate Fixed Points}}}
\author{Alexis \textsc{Cellier} (acel@itu.dk)\\Casper Brock \textsc{Petersen} (casb@itu.dk)}
\date{2014-04-18}

\begin{document}

\maketitle

\makeabstract{The purpose of this paper is the explore a different way of finding approximate fixed points for functions in $R^n$.}

\begin{multicols}{2}

\section*{Introduction}

We will look into how to use a Sperner-labeling of a triangulation of $R^n$. We can then search the triangulation for a specific function for a pan-chromatic triangle. For well-behaved functions, a pan-chromatic triangle corresponds to an approximate fixed point for the function. We wish to explore the feasibility of using this method. In algorithmic game theory a Nash equilibria of a game is a state where no player would gain anything from changing their strategy. So this is essentially a fixed point of a function, and what we hope to eventually find for normal form games with more than two players.

\section{Related Work}

In \cite{scarf73} it is shown how to do a barycentric subdivision of a simplex while maintaining a numbering of the different simplices constructed by the subdivision. The numbering serves to both identify the different simplices as well as providing a simple algorithm for finding adjacent simplices.\\

For a simplex with $n$ vertices, each barycentric subdivision consists of $n!$ subsimplices each numbered with a permutation of the integers from 1 to $n$. Finding an adjacent subsimplice can then be done by dropping a vertex, corresponding to a integer in the range. The adjacent subsimplice is then the simplice with the numbering where the chosen number is swapped with the following number (e.g. for the subsimplice 312 the simplice obtained by dropping 1 is 321).
If the integer is the last number in the permutation, the other two vertices lie on the boundary of the simplice and no replacement can be found.\\

This subdivision can be done recursively to obtain a finer grid. The numbering works in much the same way, except we now have several levels (e.g. 123-312 is the simplice 312 inside the simplice 123). Dropping a vertex is done in the same way, except when you want to drop the last vertex, 2 in the example. Dropping this vertex means we need to go outside the simplex 123 to find a replacement.\\

The general way to find a replacement is: To drop a integer $x$, find the rightmost permutation where the integer you want to remove is not the last integer, then in that permutation swap it with the integer to it's right, now named $y$. In all the permutations to the right of the permutation where the swap occurred, swap $x$ and $y$ (e.g. for the simplex 2314-1324-1243-4123 dropping the vertex 3 results in the simplex: 2314-1234-1342-4132). If all the permutations contain $x$ at the end, the other vertices lie on the boundary, and no replacement can be made.\\

The main result here is that we can compute a number of barycentric subdivisions for a simplex, resulting in $(n!)^m$ subsimplices, where $m$ is the number of recursions. The algorithm then allows us to drop a vertex from a simplex and find a replacement in constant time, as all we need to do is calculate the new permutation and look it up in a table.\\

In \cite{goncalves06} is it shown that we can also do an edgewise subdivision to divide to simplex into subsimplices of same shape and volume. Here each subsimplex is identified between 0 and $n-1$, where n is the number of subsimplices. However this approach does not give any specific algorithm of how to take a simplex, drop a vertex and find an adjacent simplex. Due to the representation of the different subsimplices such an algorithm should be possible to construct.\\

We chose to use the barycentric subdivision because the algorithm to find an adjacent simplex is very simple. While the edgewise subdivision has the advantage of producing simplices that are of equal size, the algorithm for dropping a vertex and finding a replacement would be significantly more complicated.

\section{Sperner-labeling and finding pan-chromatic polygon}

Sperner's lemma says that a Sperner-labeling (described below) of a triangulation of an $n$-dimensional simplex contains at least $(n+1)-2$ polygons labeled with $n+1$ labels (a pan-chromatic triangle, if we are in the 2-dimensional simplex).\\

The Sperner-labeling is the following:
\begin{enumerate}
  \item The vertices of the original polygon (the whole simplex) are labeled each with a different label (so if it's a $n$-simplex, you will need $n+1$ labels)
  \item All the vertices on the edges of the original polygon are labelled with one of the two labels of the original vertices of this edge.
  \item All the other vertices can be labeled randomly (or using any function, of course) using the $n+1$ labels.\\
\end{enumerate}

Now we have labeled all the vertices (found by using any triangulation, and labeled using any meaningful function that respect the Sperner-labeling) of the simplex , we have to get a way to found one pan-chromatic polygon. For doing this, we use the following algorithm:
At each step, if you have only different labeled vertices in your polygon (and if the number of vertices is not $n+1$), you add a new vertex, otherwise, you remove the oldest vertex with a label that is present in double, and, if it exist, add the complementary vertex (c.f. section 2).
We can start the algorithm from any of the polygon (the easiest way is to start from one of the original vertices), and stop once we find a $n$-polygon where all its vertices have a different label.
For finding a pan-chromatic polygon, it takes linear time in the number of polygons.

\section{An application - Estimating Nash Equilibrium of two players, two strategies games}

An application of finding an approximate fixed point is estimating the Nash Equilibrium of game like seen in \cite{ppad}. The Nash Equilibrium of a game with two or more players is a set of strategies for each player, where no player will gain anything from changing their strategy. Finding this Nash Equilibrium can be reduced to the problem of finding a fixed point of a function.\\
That function is the sum of the gain that each player gets by changing their strategy. If that sum is null, i.e. no player gains anything by changing their strategy, we found a Nash Equilibrium.\\

For doing this, you first need to describe the game by a table, (see \textsc{Figure 2}) to be able to get the utility for a given player playing a strategy, knowing the choice of the other player.
\begin{center}
  \includegraphics[scale=0.7]{bos.png}\\
  \textsc{Figure 1:} Table of the game ``Battle of the Sexes''.
\end{center}

You can represent the game strategies of the players as a triangle:

\begin{center}
  \includegraphics[scale=0.35]{triangle.png}\\
  \textsc{Figure 2:} Two players, two strategies game in a triangle.
\end{center}

So, when you pick a point in that triangle, say $(0.3, 0.5)$, it means:
\begin{itemize}
  \item the player $A$ is playing $X$ 30\% of the time, and 70\% ($1 - 0.3$) the choice $Y$;
  \item the player $B$ is playing $X$ 50\% of the time, and 50\% ($1 - 0.5$) the choice $Y$.
\end{itemize}

The grey zones in the triangle are situations that do not exist (you can not play a strategy more that 100\% of the time), so when you pick a point from this area, you bring the point back to a real situation by maximizing its values to 1 (ex: the point $(1.4, 0.5)$ become $(1, 0.5)$).\\

To calculate the gain for each player, you first calculate the expected utility:

\begin{lstlisting}
expect_utility(A, S) =
   utility[A, S][B, X] * chance(B, X)
 + utility[A, S][B, Y] * chance(B, Y)
\end{lstlisting}
where:
\begin{itemize}
  \item \texttt{expect\_utility(P, S)} means: expected utility for the player \texttt{P} to play \texttt{S} with the current strategy.
  \item \texttt{change(P, S)} means: chance for the player \texttt{P} to play \texttt{S} with the current strategy.
  \item \texttt{utility[P, S][Q, T]} means: utility for player \texttt{P} to play \texttt{S} knowing that player \texttt{Q} is playing \texttt{T}.
\end{itemize}

\noindent Once you have the 4 expected utilities, you calculate the excess utility (the gain):
\begin{lstlisting}
excess_utility(P, S) = max(0,
  expected_utility(P, S)
  - (expected_utility(P, X)
     + expected_utility(P, Y)) / 2)
\end{lstlisting}

\noindent Finally, you calculate the new strategies by adding the gain and normalize (so that \texttt{chance(P, X) + chance(P, Y) = 1}):
\begin{lstlisting}
chance(P, S) =
  normalize(chance(P, S)
            + excess_utility(P, S))
\end{lstlisting}

This calculation gives you the new strategies they should apply (if the strategies are the same, it is a Nash Equilibrium). These new strategies give a new point in the triangle. With the two points (the original and the new), you compute the angle between the vertical and the vector they form, and color the point depending of that angle.\\

Using the simplex subdivision, we get get a list of triangles (essentially a list of points) and then we are just searching a pan-chromatic triangle to estimate a Nash Equilibrium in the game.

\bibliography{report}

\end{multicols}

\end{document}
