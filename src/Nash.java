import java.util.Arrays;

public class Nash implements Updater
{
        double[][] utilities;
        int players;
        int actions;

        public void initialize(int players, int actions, double[][] utilities)
        {
                this.utilities = utilities;
                this.players = players;
                this.actions = actions;
        }

        public double[] update(double[] coordinates)
        {
                return transform(update(transform(coordinates)));
        }

        public double getEpsilon(double[] coordinates)
        {
                return getEpsilon(transform(coordinates));
        }

        public String toString(double[] coordinates)
        {
                String output = "";

                double[][] strategies = transform(coordinates);
                for (int player = 0; player < players; player++)
                {
                        output += "Player " + (player + 1) + ":\n";
                        for (int action = 0; action < actions; action++)
                                output += "\tAction " + (action + 1) + ": " + strategies[player][action] + "\n";
                }

                return output;
        }


        private double[][] transform(double[] coordinates)
        {
                double[][] strategies = new double[players][];
                for (int player = 0; player < players; player++)
                {
                        strategies[player] = new double[actions];
                        double sum = 0.0;
                        for (int action = 0; action < actions - 1; action++)
                                sum += (strategies[player][action] = coordinates[player * (actions - 1) + action]);
                        strategies[player][actions - 1] = 1.0 - sum;

                        sum = 0.0;
                        double max = 1.0 / players;
                        for (int action = 0; action < actions; action++)
                        {
                                if (strategies[player][action] > max)
                                        strategies[player][action] =  max;
                                if (strategies[player][action] > max - sum)
                                        strategies[player][action] = Math.max(0, max - sum);
                                sum += strategies[player][action];
                        }
                        for (int action = 0; action < actions; action++)
                                strategies[player][action] *= players;
                }

                return strategies;
        }

        private double[] transform(double[][] strategies)
        {
                double[] coordinates = new double[(players * (actions - 1)) + 1];
                double sum = 0.0;
                for (int player = 0; player < players; player++)
                        for (int action = 0; action < actions - 1; action++)
                                sum += (coordinates[player * (actions - 1) + action] = strategies[player][action] / players);
                coordinates[players * (actions - 1)] = 1 - sum;
                return coordinates;
        }

        private double[][] update(double[][] strategies)
        {
                double[][] expectUtilities = new double[strategies.length][];
                for (int player = 0; player < strategies.length; player++)
                {
                        expectUtilities[player] = new double[strategies[player].length];
                        for (int action = 0; action < strategies[player].length; action++)
                        {
                                expectUtilities[player][action] = 0;

                                for (int i = 0; i < Math.pow(actions, players); i++)
                                {
                                        int actionSet = i;
                                        int oldActionSet = actionSet;
                                        actionSet += - (actionSet / Math.pow(actions, player) % actions) + (action * Math.pow(actions, player));
                                        if (actionSet == oldActionSet)
                                                continue;
                                        double coef = 1;
                                        int savedActionSet = actionSet;
                                        if (savedActionSet >= Math.pow(actions, players))
                                                continue;
                                        for (int j = 0; j < players; j++)
                                        {
                                                if (j != player)
                                                        coef *= strategies[j][actionSet % actions];
                                                actionSet /= actions;
                                        }
                                        expectUtilities[player][action] += utilities[player][savedActionSet] * coef;
                                }
                        }
                }

                double[][] excessUtilities = new double[strategies.length][];
                for (int player = 0; player < strategies.length; player++)
                {
                        excessUtilities[player] = new double[strategies[player].length];
                        for (int action = 0; action < strategies[player].length; action++)
                        {
                                double sum = 0;
                                for (int i = 0; i < strategies[player].length; i++)
                                        sum += expectUtilities[player][i];

                                excessUtilities[player][action] = Math.max(0, expectUtilities[player][action] - (sum / actions));
                        }
                }

                for (int player = 0; player < strategies.length; player++)
                {
                        double sum = 0;
                        for (int action = 0; action < strategies[player].length; action++)
                        {
                                strategies[player][action] += excessUtilities[player][action];
                                sum += strategies[player][action];
                        }
                        for (int action = 0; action < strategies[player].length; action++)
                                strategies[player][action] /= sum;
                }

                return strategies;
        }

        private double getEpsilon(double[][] strategies)
        {
                double[][] expectUtilities = new double[strategies.length][];
                for (int player = 0; player < strategies.length; player++)
                {
                        expectUtilities[player] = new double[strategies[player].length];
                        for (int action = 0; action < strategies[player].length; action++)
                        {
                                expectUtilities[player][action] = 0;

                                for (int i = 0; i < Math.pow(actions, players); i++)
                                {
                                        int actionSet = i;
                                        int oldActionSet = actionSet;
                                        actionSet += - (actionSet / Math.pow(actions, player) % actions) + (action * Math.pow(actions, player));
                                        if (actionSet == oldActionSet)
                                                continue;
                                        double coef = 1;
                                        int savedActionSet = actionSet;
                                        if (savedActionSet >= Math.pow(actions, players))
                                                continue;
                                        for (int j = 0; j < players; j++)
                                        {
                                                if (j != player)
                                                        coef *= strategies[j][actionSet % actions];
                                                actionSet /= actions;
                                        }
                                        expectUtilities[player][action] += utilities[player][savedActionSet] * coef;
                                }
                        }
                }

                double[][] excessUtilities = new double[strategies.length][];
                for (int player = 0; player < strategies.length; player++)
                {
                        excessUtilities[player] = new double[strategies[player].length];
                        for (int action = 0; action < strategies[player].length; action++)
                        {
                                double sum = 0;
                                for (int i = 0; i < strategies[player].length; i++)
                                        sum += expectUtilities[player][i];

                                excessUtilities[player][action] = Math.max(0, expectUtilities[player][action] - (sum / actions));
                        }
                }

                double max = 0.0;
                for (int player = 0; player < strategies.length; player++)
                        for (int action = 0; action < strategies[player].length; action++)
                                if (max < excessUtilities[player][action])
                                        max = excessUtilities[player][action];

                return max;
        }


}
