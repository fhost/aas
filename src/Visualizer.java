import java.util.*;
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.BorderLayout;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.Scanner;

import javax.swing.JComponent;
import javax.swing.JFrame;

// TODO: highlight without reprint evreything

public class Visualizer
{
  private static class Point
  {
    public double x;
    public double y;
    public double nx;
    public double ny;
    public Color  color = null;

    public Point(double x, double y)
    {
      this.x = x;
      this.y = y;
    }

    public Point add(Point p)
    {
      return new Point(this.x + p.x, this.y + p.y);
    }

    public Point div(double div)
    {
      return new Point(this.x / div, this.y / div);
    }

    @Override
    public boolean equals(Object o)
    {
      if (o == null)
        return false;
      Point other = (Point) o;
      return (x == other.x && y == other.y);
    }
  }

  private static class Shape
  {
    public Point p1 = null;
    public Point p2 = null;
    public Point p3 = null;
    public int vertices = 0;

    public Shape(Point one, Point two, Point three)
    {
      p1 = one;
      p2 = two;
      p3 = three;
      for (Point p : Arrays.asList(p1, p2, p3))
        if (p != null)
          vertices++;
    }

    public char getVertex(Point point)
    {
    	if (point.equals(p1))
          return '1';
    	else if (point.equals(p2))
          return '2';
    	else
          return '3';
    }

    public void draw(Graphics2D g, Color color, double scaling)
    {
      g.setColor(color);
      g.setStroke(new BasicStroke((float) (scaling/scale)));

      if (p1 != null && p2 != null)
        g.draw(new Line2D.Double(p1.x, p1.y, p2.x, p2.y));
      if (p1 != null && p3 != null)
        g.draw(new Line2D.Double(p1.x, p1.y, p3.x, p3.y));
      if (p2 != null && p3 != null)
        g.draw(new Line2D.Double(p2.x, p2.y, p3.x, p3.y));
      double radius = scaling/scale + 3.0;
      for (Point p : Arrays.asList(p1, p2, p3))
        if (p != null)
        {
          g.setColor(p.color);
          g.fill(new Ellipse2D.Double(p.x - radius, p.y - radius, radius * 2, radius * 2));
          //g.draw(new Line2D.Double(p.x, p.y, p.nx, p.ny));
        }
    }

    public boolean color()
    {
      List<Color> colors = new ArrayList<Color>();
      for (Point p : Arrays.asList(p1, p2, p3))
        if (p != null && (! colors.contains(p.color)))
          colors.add(p.color);

      return (colors.size() == vertices);
    }
  }

  private static class Constructor
  {
    public String name;
    public Shape shape;
    public boolean border;

    public Constructor(String name, Point one, Point two, Point three, boolean border)
    {
      this.name = name;
      this.shape = new Shape(one, two, three);
      this.border = border;
    }

    public void addShapes(Map<String, Shape> shapes)
    {
      shapes.put(name, shape);
      if (border != false)
      {
        String last = name.substring(name.length() - 1, name.length());
        String key = name.replace(last, "");
        if (last.equals("1"))
          shapes.put(key, new Shape(null, shape.p2, shape.p3));
        else if (last.equals("2"))
          shapes.put(key, new Shape(shape.p1, null, shape.p3));
        else if (last.equals("3"))
          shapes.put(key, new Shape(shape.p1, shape.p2, null));
      }
    }

    public boolean isBorder(char last)
    {
      if (border == true)
      {
        if (name.length() != 0)
          return name.toCharArray()[name.length() - 1] == last;
        return true;
      }
      return false;
    }
  }

  private static int getSize(int depth)
  {
    for (int size = 0, level = 0;;depth--)
      if (depth == 0)
        return size;
      else
        size += Math.pow(6, level++);
  }

  private static Map<String, Shape> generateShapes(int size, int depth)
  {
    double x1 = 0;
    double y1 = size * Math.sqrt(0.75);
    double x2 = size;
    double y2 = size * Math.sqrt(0.75);
    double x3 = size / 2;
    double y3 = 0;

    Map<String, Shape> shapes = new HashMap<String, Shape>(getSize(depth + 1));
    Queue<Constructor> queue = new LinkedList<Constructor>();

    Point p1 = new Point(x1, y1);
    Point p2 = new Point(x2, y2);
    Point p3 = new Point(x3, y3);

    double[] pos = toPositions(nash(toStrategies(convert(p1, size)), gameTable));
    System.out.printf("%f - %f => %f - %f => %f - %f\n", p1.x, p1.y, convert(p1, size)[0], convert(p1, size)[1], pos[0], pos[1]);
    pos = toPositions(nash(toStrategies(convert(p2, size)), gameTable));
    System.out.printf("%f - %f => %f - %f => %f - %f\n", p2.x, p2.y, convert(p2, size)[0], convert(p2, size)[1], pos[0], pos[1]);
    pos = toPositions(nash(toStrategies(convert(p3, size)), gameTable));
    System.out.printf("%f - %f => %f - %f => %f - %f\n", p3.x, p3.y, convert(p3, size)[0], convert(p3, size)[1], pos[0], pos[1]);


    Constructor triangle = new Constructor("", p1, p2, p3, true);

    queue.add(triangle);

    // generate shape constructors
    for (int i = getSize(depth); i > 0; i--)
    {
      triangle = queue.poll();
      Point p123 = triangle.shape.p1.add(triangle.shape.p2).add(triangle.shape.p3).div(3);
      Point p12 = triangle.shape.p1.add(triangle.shape.p2).div(2);
      Point p13 = triangle.shape.p1.add(triangle.shape.p3).div(2);
      Point p23 = triangle.shape.p2.add(triangle.shape.p3).div(2);

      queue.add(new Constructor(triangle.name + "123", triangle.shape.p1, p12, p123, triangle.isBorder('3')));
      queue.add(new Constructor(triangle.name + "132", triangle.shape.p1, p123, p13, triangle.isBorder('2')));
      queue.add(new Constructor(triangle.name + "213", p12, triangle.shape.p2, p123, triangle.isBorder('3')));
      queue.add(new Constructor(triangle.name + "231", p123, triangle.shape.p2, p23, triangle.isBorder('1')));
      queue.add(new Constructor(triangle.name + "312", p13, p123, triangle.shape.p3, triangle.isBorder('2')));
      queue.add(new Constructor(triangle.name + "321", p123, p23, triangle.shape.p3, triangle.isBorder('1')));
    }

    // add points
    char[] point = new char[depth];
    Arrays.fill(point, '1');
    shapes.put(new String(point), new Shape(p1, null, null));
    Arrays.fill(point, '2');
    shapes.put(new String(point), new Shape(null, p2, null));
    Arrays.fill(point, '3');
    shapes.put(new String(point), new Shape(null, null, p3));

    // add lines and triangles
    while (! queue.isEmpty())
      queue.poll().addShapes(shapes);

    return shapes;
  }

  public static ArrayList<Shape> search(Map<String, Shape> shapes, int depth)
  {
    ArrayList<Shape> path = new ArrayList<Shape>();
    LinkedList<Point> points = new LinkedList<Point>();
    String next = "";
    Shape current;

    // initizalization
    for (int i = 0; i < depth; i++)
      next += "2";

    current = shapes.get(next);
    path.add(current);

    int vertices = 3;

    while (! (current.vertices == 3 && current.color()))
    {
      for (Point point : Arrays.asList(current.p1, current.p2, current.p3))
        if (point != null && ! points.contains(point))
          points.add(point);

      if (current.vertices == vertices && ! current.color())
        next = dropVertex(next, current.getVertex(points.remove(points.get(0).color == points.get(current.vertices - 1).color ? 0 : 1)), depth);
      else
        next = addVertex(next, depth);

      current = shapes.get(next);
      path.add(current);
      vertices = current.vertices;
    }

    return path;
  }

  static double[][] playerA = new double[][]
    {
      { 3.0, 0.0 },
      { 0.0, 2.0 }
    };
  static double[][] playerB = new double[][]
    {
      { 2.0, 0.0 },
      { 0.0, 3.0 }
    };
  private static GameTable gameTable = new GameTable(playerA, playerB);
  private static ShapeCanvas canvas;
  private static double scale = .6;

  public static void main(String[] args)
  {
    int size = 512;
    int depth;

    System.out.println("How many levels do you want to draw?");
    Scanner s = new Scanner(System.in);

    while (true)
    {
      try
      {
        depth = s.nextInt();
        if (depth < 1)
          System.out.println("You put in an incorrect number, must be an integer higher than 0");
        else
          break;
      }
      catch (NoSuchElementException e)
      {
        System.out.println("You must input an integer higher than 0");
        s.next();
      }
    }

    Map<String, Shape> shapes = generateShapes(size, depth);
    colorize(shapes);
    ArrayList<Shape> path = search(shapes, depth);
    // ArrayList<Shape> path = new ArrayList<Shape>();

    canvas = new ShapeCanvas(shapes, path);

    JFrame frame = new JFrame();
    frame.setLayout(new BorderLayout());
    frame.getContentPane().add(canvas, BorderLayout.CENTER);
    frame.setSize(size, size);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.setVisible(true);
  }

  public static void swap(char[] triangle, int x, int y)
  {
    triangle[x] ^= triangle[y];
    triangle[y] ^= triangle[x];
    triangle[x] ^= triangle[y];
  }

  private static String addVertex(String name, int depth)
  {
    char[] shape = name.toCharArray();
    int vertices = shape.length / depth;
    char missing;

    if (vertices == 1)
    {
      char[] result = new char[shape.length * 2];
      missing = (char) ('0' + ((shape[0] - '0') % 3 + 1));

      for (int i = 0; i < shape.length; i++)
      {
        result[i * 2] = shape[i];
        result[i * 2 + 1] = missing;
      }
      return new String(result);
    }

    else if (vertices == 2)
    {
      char[] result = new char[shape.length + shape.length / 2];
      missing = (char) (6 - (shape[0] - '0') - (shape[1] - '0') + '0');
      for(int i = 0, k = 0; k < shape.length; i += 3, k += 2)
      {
        result[i] = shape[k];
        result[i + 1] = shape[k + 1];
        result[i + 2] = missing;
      }
      return new String(result);
    }
    else
    {
      System.out.println("Something is wrong with the name");
      return name;
    }
  }

  private static String dropVertex(String name, char vertex, int depth)
  {
    char[] shape = name.toCharArray();
    int vertices = shape.length / depth;
    int i;

    if (vertices <= 3)
    {
        for (i = shape.length - vertices; i >= 0; i -= vertices)
          if (shape[i + vertices - 1] != vertex)
              break;
        if (i < 0)
          return (new String(shape)).replace("" + vertex, "");
        if (shape[i] != vertex)
          i += 1;
        char c = shape[i + 1];
        swap(shape, i, i + 1);
        for (i += vertices - (i % vertices); i < shape.length; i += vertices)
          if (shape[i] == c || shape[i] == vertex)
            if (shape[i + 1] == vertex)
              swap(shape, i, i + 1);
            else
              swap(shape, i, i + 2);
          else
            swap(shape, i + 1, i + 2);
        return new String(shape);
    }
    else // something wrong
    {
    	System.out.println("Something is wrong with the length of the name");
    	return name;
    }
  }

  private static void colorize(Map<String, Shape> shapes)
  {
    for (Shape shape : shapes.values())
      for (Point point : Arrays.asList(shape.p1, shape.p2, shape.p3))
      {
        int size = 512;

        if (point != null && point.color == null)
        {
          double[] positions = convert(point, size);
          double[] strategies = toStrategies(positions);
          double[] newStrategies = nash(strategies, gameTable);
          double[] newPositions = toPositions(newStrategies);
          Point newPoint = convert(newPositions, size);
          point.nx = newPoint.x;
          point.ny = newPoint.y;

          if (positions[0] == newPositions[0] && positions[1] == newPositions[1])
          {
            if (positions[0] == 0 && positions[1] == 0)
              point.color = Color.RED;
            else if (positions[0] + positions[1] == 1)
              point.color = Color.BLUE;
            else if (positions[0] == 0)
              point.color = Color.GREEN;
            else if (positions[1] == 0)
              point.color = Color.BLUE;
            continue;
          }

          double angle = (180.0 / Math.PI) * Math.atan2(point.x - point.nx, point.ny - point.y);

          if (angle < 60.0 && angle > -60)
            point.color = Color.GREEN;
          if (angle <= -60)
            point.color = Color.RED;
          if (angle >= 60)
            point.color = Color.BLUE;
        }
      }
  }

  private static class ShapeCanvas extends JComponent implements MouseListener,
                                                                 MouseMotionListener,
                                                                 MouseWheelListener
  {
    private static final long serialVersionUID = 1L;
    Map<String, Shape> shapes = null;
    Shape currentHighlight = null;
    Shape previousHighlight = null;
    ArrayList<Shape> path;
    int i = 0;

    private double translateX = 100;
    private double translateY = 100;
    private int lastOffsetX;
    private int lastOffsetY;

    public ShapeCanvas(Map<String, Shape> shapes, ArrayList<Shape> path)
    {
      super();
      this.shapes = shapes;
      setOpaque(true);
      setDoubleBuffered(true);
      this.path = path;
      this.addMouseListener(this);
      this.addMouseMotionListener(this);
      this.addMouseWheelListener(this);
    }

    @Override
    public void paint(Graphics _g)
    {
      AffineTransform tx = new AffineTransform();
      tx.translate(translateX, translateY);
      tx.scale(scale, scale);
      Graphics2D g = (Graphics2D) _g;
      g.setColor(Color.WHITE);
      g.fillRect(0, 0, getWidth(), getHeight());
      g.setTransform(tx);
      g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                         RenderingHints.VALUE_ANTIALIAS_ON);
      g.setColor(Color.BLACK);
      g.setStroke(new BasicStroke((float) (.2/scale)));

      // draw all the shapes
      for (Shape shape : shapes.values())
        shape.draw(g, Color.BLACK, 0.2);

      // highlight the previous shape
      if (previousHighlight != null)
          previousHighlight.draw(g, Color.DARK_GRAY, 1.8);

      // highlight the current shape
      if (currentHighlight != null)
        currentHighlight.draw(g, Color.MAGENTA, 1.8);
    }

    public void mouseClicked(MouseEvent arg0)
    {
      if (i < path.size())
      {
        previousHighlight = currentHighlight;
        currentHighlight = path.get(i);
        i++;
        repaint();
      }
    }

    public void mousePressed(MouseEvent e)
    {
      lastOffsetX = e.getX();
      lastOffsetY = e.getY();
    }

    public void mouseDragged(MouseEvent e)
    {
      int newX = e.getX() - lastOffsetX;
      int newY = e.getY() - lastOffsetY;
      lastOffsetX += newX;
      lastOffsetY += newY;
      translateX += newX;
      translateY += newY;
      canvas.repaint();
    }

    public void mouseWheelMoved(MouseWheelEvent e)
    {
      if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
        double scaleT = Math.pow(0.9, e.getWheelRotation());
        scale *= scaleT;
        translateX = e.getX() * (1 - scale) + translateX * scale;
        translateY = e.getY() * (1 - scale) + translateY * scale;
        canvas.repaint();
      }
    }

    //Unused mouseEvents
    public void mouseEntered(MouseEvent arg0) {}
    public void mouseExited(MouseEvent arg0) {}
    public void mouseReleased(MouseEvent arg0) {}
    public void mouseMoved(MouseEvent arg0) {}
  }

  private static class GameTable
  {
    /*
     * For p, expU and excU:
     * [0] = player 1 strategy 1
     * [1] = player 1 strategy 2
     * [2] = player 2 strategy 1
     * [3] = player 2 strategy 2
     *
     */
    double[][] player1U; // utilities for player 1 (set at construction)
    double[][] player2U; // utilities for player 2 (set at construction)
    double[] p;          // probabilities (set at calculation)
    double[] expU;
    double[] excU;

    public GameTable(double[][] player1U, double[][] player2U)
    {
      this.player1U = player1U;
      this.player2U = player2U;
      p = new double[4];
      expU = new double[4];
      excU = new double[4];
    }

    public void calculate(double[] p)
    {
      this.p = p;

      expU[0] = player1U[0][0] * p[2] + player1U[1][0] * p[3];

      expU[1] = player1U[0][1] * p[2] + player1U[1][1] * p[3];

      expU[2] = player2U[0][0] * p[0] + player2U[1][0] * p[1];

      expU[3] = player2U[0][1] * p[0] + player2U[1][1] * p[1];

      excU[0] = Math.max(0, expU[0] - ((expU[0] + expU[1]) / 2.0));
      excU[1] = Math.max(0, expU[1] - ((expU[0] + expU[1]) / 2.0));
      excU[2] = Math.max(0, expU[2] - ((expU[2] + expU[3]) / 2.0));
      excU[3] = Math.max(0, expU[3] - ((expU[2] + expU[3]) / 2.0));
    }

    public double getExcUtil(int i)
    {
      return excU[i];
    }
  }

  static Point convert(double[] positions, double size)
  {
    double x = positions[0];
    double y = positions[1];

    y = 1 - y;
    x -= (y - 1) * 0.5;
    y *= Math.sqrt(0.75);

    return new Point(x * size, y * size);
  }

  static double[] convert(Point point, double size)
  {
    double[] positions = new double[]
    {
      point.x / size,
      point.y / size
    };

    positions[1] /= Math.sqrt(0.75);
    positions[0] += (positions[1] - 1) * 0.5;
    positions[1] = 1 - positions[1];

    if (positions[0] > 0.5)
      positions[0] = 0.5;

    if (positions[1] > 0.5)
      positions[1] = 0.5;

    return positions;
  }

  static double[] toStrategies(double[] positions)
  {
    double[] strategies = new double[4];

    for (int i = 0; i < 2; i++)
    {
      strategies[(i * 2)] = positions[i] * 2.0;
      strategies[(i * 2) + 1] = 1.0 - strategies[(i * 2)];
    }

    return strategies;
  }

  static double[] toPositions(double[] strategies)
  {
    return new double[]
    {
      strategies[0] / 2.0,
      strategies[2] / 2.0
    };
  }

  static void normalize(double[] strategies)
  {
    for (int p = 0; p < 2; p++)
      for (int i = 0; i < 2; i++)
        strategies[(p * 2) + i] /= (strategies[(p * 2)] + strategies[(p * 2) + 1]);
  }

  static double[] nash(double[] strategies, GameTable gameTable)
  {
    gameTable.calculate(strategies);

    double[] newStrategies = new double[4];
    for (int i = 0; i < 4; i++)
      newStrategies[i] = gameTable.getExcUtil(i) + strategies[i];
    normalize(newStrategies);

    return newStrategies;
  }
}
