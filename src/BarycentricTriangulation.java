import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.LinkedList;

public class BarycentricTriangulation implements Triangulation
{
        private List<List<Integer>> current = new ArrayList<List<Integer>>();
        private int size;
        private Map<String, List<Vertex>> polygons;
        private int depth;
        private int n;
        private Queue<Integer> missings = new LinkedList<Integer>();

        public BarycentricTriangulation(int depth)
        {
                this.depth = depth;
        }

        public void initialize(List<Vertex> vertices, int n, int start)
        {
                this.n = n;

                for (int i = 0; i < depth; i++)
                        current.add(new ArrayList<Integer>(Arrays.asList(start)));
                vertices.addAll(generate());
                for (int i = (start + 1) % n; i != start; i = (i + 1) % n)
                        missings.add(i);
        }

        public void dropVertex(List<Vertex> vertices, Vertex drop)
        {
                vertices.remove(drop);

                // System.out.print("drop: ");
                // System.out.println(drop);

                Integer dropName = 0;
                for (Vertex vertex : generate())//polygons.get(getKey()))
                {
                        // System.out.println(vertex);
                        if (vertex.equals(drop))
                                break;
                        else
                                dropName++;
                }

                Integer[] tmp = new Integer[current.get(0).size()];
                current.get(0).toArray(tmp);
                Arrays.sort(tmp);
                dropName = tmp[dropName];

                int part;
                for (part = depth - 1; part >= 0; part--)
                        if (current.get(part).get(current.get(part).size() - 1) != dropName)
                                break;

                if (part < 0)
                {
                        for (List<Integer> parts : current)
                                parts.remove(dropName);
                        missings.add(dropName);
                        return;
                }

                Integer compName = null;
                for (int i = 0; i < current.get(part).size(); i++)
                        if (current.get(part).get(i) == dropName)
                        {
                                compName = current.get(part).get(i + 1);
                                 break;
                        }

                for (int i = part; i < depth; i++)
                {
                        int compPos = current.get(i).indexOf(compName);
                        current.get(i).set(current.get(i).indexOf(dropName), compName);
                        current.get(i).set(compPos, dropName);
                }

                for (Vertex vertex : generate())//polygons.get(getKey()))
                        if (! vertices.contains(vertex))
                        {
                                vertices.add(vertex);
                                return;
                        }
        }

        public void addVertex(List<Vertex> vertices)
        {
                // int missing = (current.get(0).get(current.get(0).size() - 1) + 1) % n;

                int missing = missings.poll();

                for (List<Integer> part : current)
                        part.add(missing);

                for (Vertex vertex : generate()) //polygons.get(getKey()))
                        if (! vertices.contains(vertex))
                        {
                                vertices.add(vertex);
                                return;
                        }
        }


        private String getKey()
        {
                StringBuilder sb = new StringBuilder(size + "-");
                for (List<Integer> part : current)
                {
                        for (Integer subpart : part)
                                sb.append(subpart.toString() + ",");
                        sb.setCharAt(sb.length() - 1, '-');
                }
                return sb.deleteCharAt(sb.length() - 1).toString();
        }

        private List<Vertex> generate()
        {
                Vertex[] polygon = new Vertex[n];
                for (int i = 0; i < n; i++)
                {
                        double[] coordinates = new double[n];
                        coordinates[i] = 1;
                        Vertex vertex = new Vertex(coordinates);
                        polygon[i] = vertex;
                }

                for (int i = 0; i < depth; i++)
                {
                        Vertex[] newPolygon = new Vertex[n];
                        List<Vertex> seens = new ArrayList<Vertex>();
                        for (int j = 0; j < current.get(0).size(); j++)
                        {
                                seens.add(polygon[current.get(i).get(j)]);
                                newPolygon[current.get(i).get(j)] = average(seens);
                        }
                        polygon = newPolygon;
                }

                ArrayList<Vertex> result = new ArrayList<Vertex>();
                for(Vertex v : polygon)
                	if(v != null)
                		result.add(v);
                return result;
        }

        private Vertex average(List<Vertex> vertices)
        {
                Vertex average = new Vertex(n);
                for (int i = 0; i < n; i++)
                {
                        double total = 0;
                        for (Vertex vertex : vertices)
                                total += vertex.coordinates[i];
                        average.coordinates[i] = total / vertices.size();
                }

                return average;
        }

        private class Polygon
        {
                public List<Vertex> vertices;
                public String key;

                public Polygon()
                {
                        vertices = new ArrayList<Vertex>();
                }


                public Polygon(int n)
                {
                        vertices = new ArrayList<Vertex>(n);
                }

                public Polygon(List<Vertex> vertices, String key)
                {
                        this.vertices = vertices;
                        this.key = key;
                }
        }
}
