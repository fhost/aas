public interface Updater
{
        public void initialize(int players, int actions, double[][] utilities);
	public double[] update(double[] coordinates);
        public double getEpsilon(double[] coordinates);
        public String toString(double[] coordinates);
}
