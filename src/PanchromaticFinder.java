import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class PanchromaticFinder
{
        int n;
        Updater updater;
        Triangulation triangulation;
        boolean nashFound = false;
        Vertex vertex = null;
        int start;
        public double epsilon = 0.0;

        public PanchromaticFinder(int players, int actions, double[][] utilities, Updater updater, Triangulation triangulation, int start)
        {
                this.n = players * (actions - 1) + 1;
                this.updater = updater;
                this.updater.initialize(players, actions, utilities);
                this.triangulation = triangulation;
                this.start = start;
        }

        public List<Vertex> search()
        {
                List<Vertex> vertices = new LinkedList<Vertex>();
                triangulation.initialize(vertices, n, start);

                // System.out.println("======");
                // for (Vertex v : vertices)
                // System.out.println(v);
                // System.out.println("======");

                while (! (panchromatic(vertices) && vertices.size() == n))
                {
                        if (nashFound)
                        {
                                System.out.println(0.0);
                                epsilon = 0.0;
                                return Arrays.asList(vertex);
                        }
                        if (vertices.size() == n || vertex != null)
                                triangulation.dropVertex(vertices, vertex);
                        else
                                triangulation.addVertex(vertices);
                }

                // System.out.println(updater.toString(getCenter(n, vertices).coordinates));
                epsilon = updater.getEpsilon(getCenter(n, vertices).coordinates);

                return vertices;
        }

        private boolean panchromatic(List<Vertex> vertices)
        {
                List<Integer> colors = new ArrayList<Integer>(n);
                ListIterator<Vertex> vertexIt = vertices.listIterator(vertices.size());

                // System.out.println("------");
                // for (Vertex v : vertices)
                //         System.out.println(v.label);
                // System.out.println("------");

                while (vertexIt.hasPrevious())
                {
                        vertex = vertexIt.previous();
                        if (vertex.label == -1)
                        	if (colorize(vertex, updater.update(vertex.coordinates)))
                        	{
                        		nashFound = true;
                        		return false;
                        	}
                        if (colors.contains(new Integer(vertex.label)))
                                return false;
                        colors.add(new Integer(vertex.label));
                }
                vertex = null;
                return true;
        }

        private boolean colorize(Vertex vertex, double[] newCoordinates)
        {
                double min = Double.POSITIVE_INFINITY;

                // System.out.println("coordonates");
                // System.out.println(Arrays.toString(vertex.coordinates));
                // System.out.println(Arrays.toString(newCoordinates));

                for (int i = 0; i < newCoordinates.length; i++)
                {
                        if (min > newCoordinates[i] - vertex.coordinates[i])
                        {
                                min = newCoordinates[i] - vertex.coordinates[i];
                                vertex.label = i;
                        }
                }

            	return min == 0.0;
        }

        private static Vertex getCenter(int n, List<Vertex> vertices)
        {
                Vertex center = new Vertex(n);
                for (Vertex vertex : vertices)
                        for (int i = 0; i < n; i++)
                                center.coordinates[i] += vertex.coordinates[i] / vertices.size();
                return center;
        }
}
