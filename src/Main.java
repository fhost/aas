import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import edu.stanford.multiagent.gamer.CovariantGame;
import edu.stanford.multiagent.gamer.BattleOfTheSexes;
import edu.stanford.multiagent.gamer.Game;

public class Main {

	public static void main(String[] args) {

		int players = 3;
		int actions = 2;
		int iterations = 10;
		Game cg = null;

		try {
			// cg = new BattleOfTheSexes();
			cg = new CovariantGame();
			cg.setParameter("players", new Long(players));
			cg.setParameter("actions", new Vector<String>(Arrays.asList(Integer.toString(actions))));
			cg.setParameter("r", 0.6);
			cg.initialize();
			cg.doGenerate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		double[][] utilities = getUtilities(cg, players, actions);
		// double[][] utilities = new double[][]{{1.0,0.0,0.0,1.0}, {0.0,1.0,1.0,0.0}};

		
		String[] data = new String[iterations];
		for(int i = 1; i <= iterations; i++) {
			PanchromaticFinder pf = new PanchromaticFinder(players, actions, utilities, new Nash(), new BarycentricTriangulation(i), 0);
			List<Vertex> found = pf.search();
			data[i - 1] = "" + i + " " + pf.epsilon; 
			System.out.println("Finished iteration " + i);
		}
		writeToFile(data);
		// for (Vertex v : found)
		// 	System.out.println(v);
		// System.out.println(getCenter(players * (actions - 1) + 1, found));
		// if (found.size() == 1)
		// {
		//   Updater nash = new Nash();
		//   nash.initialize(players, actions, utilities);
		//   System.out.println(new Vertex(nash.update(found.get(0).coordinates)));
		// }
	}
	
	private static void writeToFile(String[] data)
	{
		try {  
			File file = new File("data/data.txt");
 
			// if file exists deletes it then creates it again
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
			
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for(String s : data) {
				bw.write(s);
				bw.write("\n");
			}
			bw.close();
 
			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static double[][] getUtilities(Game game, int players, int actions)
	{
		double[][] utilities = new double[players][];
		for (int player = 0; player < players; player++)
			utilities[player] = new double[(int) Math.pow(actions, players)];

		for (int actionSet = 0; actionSet < Math.pow(actions, players); actionSet++)
		{
			int currentActionSet = actionSet;
			int[] actionArray = new int[players];
			for (int player = 0; player < players; player++)
			{
				actionArray[player] = (currentActionSet % actions) + 1;
				currentActionSet /= actions;
			}

			for (int player = 0; player < players; player++)
				utilities[player][actionSet] = game.getPayoff(actionArray, player);
		}

		for (int player = 0; player < players; player++)
			System.out.println(Arrays.toString(utilities[player]));

		return utilities;
	}

}
