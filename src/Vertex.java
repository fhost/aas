import java.util.Arrays;

public class Vertex
{
  public double[] coordinates;
  public int label = -1;

  private double epsilon = 0.0000000001;

  public Vertex(int n)
  {
          this.coordinates = new double[n];
  }

  public Vertex(double[] coordinates)
  {
          this.coordinates = Arrays.copyOf(coordinates, coordinates.length);
  }

  @Override
  public boolean equals(Object o)
  {
          if (o == null || ! (o instanceof Vertex))
                  return false;
          Vertex other = (Vertex) o;
          for (int i = 0; i < coordinates.length; i++)
                  if (Math.abs(other.coordinates[i] - coordinates[i]) > epsilon)
                          return false;
          return true;
  }

  @Override
  public String toString()
  {
	  String result = "";
	  for(double d : coordinates)
		  result += d + ", ";
	  return result;
  }

}
