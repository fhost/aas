import java.util.*;

public class Test
{
	private static class GameTable
	{
		/*
		 * For p, expU and excU:
		 * [0] = player 1 strategy 1
		 * [1] = player 1 strategy 2
		 * [2] = player 2 strategy 1
		 * [3] = player 2 strategy 2
		 *
		 * for u:
		 * [0] = player 1 strategy 1, player 2 strategy 1
		 * [1] = player 2 strategy 1, player 1 strategy 1
		 * [2] = player 1 strategy 1, player 2 strategy 2
		 * [3] = player 2 strategy 1, player 1 strategy 2
		 * [4] = player 1 strategy 2, player 1 strategy 1
		 * [5] = player 2 strategy 2, player 1 strategy 1
		 * [6] = player 1 strategy 2, player 1 strategy 2
		 * [7] = player 2 strategy 2, player 1 strategy 2
		 */
		double[] u; //utilities (set at construction)
		double[] p; //probabilities (set at calculation)
		double[] expU;
		double[] excU;

		public GameTable(double[] u)
		{
			this.u = u;
			p = new double[4];
			expU = new double[4];
			excU = new double[4];
		}

		public void calculate(double[] p)
		{
			this.p = p;
			expU[0] = p[0] * (u[0] + u[2]);
			expU[1] = p[1] * (u[4] + u[6]);
			expU[2] = p[2] * (u[1] + u[3]);
			expU[3] = p[3] * (u[5] + u[7]);

			excU[0] = Math.max(0, ((expU[0] - expU[1]) / 2.0));
			excU[1] = Math.max(0, ((expU[1] - expU[0]) / 2.0));
			excU[2] = Math.max(0, ((expU[2] - expU[3]) / 2.0));
			excU[3] = Math.max(0, ((expU[3] - expU[2]) / 2.0));
		}

		public double getExcUtil(int i)
		{
			return excU[i];
		}
	}
	
	private class Triangle
	{
		public String name;
		public Point p1;
		public Point p2;
		public Point p3;
		public boolean border;

		public Triangle(String name, Point one, Point two, Point three, boolean border)
		{
			this.name = name;
			p1 = one;
			p2 = two;
			p3 = three;
			this.border = border;
		}
	}

	private class Shape
	{
		public Point p1;
		public Point p2;
		public Point p3;

		public Shape(Point one, Point two, Point three)
		{
			p1 = one;
			p2 = two;
			p3 = three;
		}
		public Shape(Point one, Point two)
		{
			p1 = one;
			p2 = two;
		}
		public Shape(Point one)
		{
			p1 = one;
		}
	}

	private class Point
	{
		public double x;
		public double y;

		public Point(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public Point add(Point p)
		{
			return new Point(this.x + p.x, this.y + p.y);
		}

		public Point div(double div)
		{
			return new Point(this.x / div, this.y / div);
		}
	}



	private static boolean isBorder(char[] name, char out)
	{
		if (name.length != 0)
			return name[name.length - 1] == out;
		return true;
	}

	public int getSize(int deepth)
	{
		for (int size = 0, level = 0;;deepth--)
			if (deepth == 0)
				return size;
			else
				size += Math.pow(6, level++);
	}

	public Test()
	{
		// to get from cmdline
		double x1 = 0;
		double y1 = 512-50;
		double x2 = 512;
		double y2 = 512-50;
		double x3 = 256;
		double y3 = 512-50-512*Math.sqrt(.75);
		int deepth = 3;

		Map<String, Shape> objects = new HashMap<String, Shape>((int)(3 + ((deepth + 1) * 3) + Math.pow(6, deepth)));
		Queue<Triangle> queue = new LinkedList<Triangle>();

		Point p1 = new Point(x1, y1);
		Point p2 = new Point(x2, y2);
		Point p3 = new Point(x3, y3);
		Triangle triangle = new Triangle("", p1, p2, p3, true);

		queue.add(triangle);

		for (int size = getSize(deepth); size > 0; size--)
		{
			triangle = queue.poll();
			Point p123 = triangle.p1.add(triangle.p2).add(triangle.p3).div(3);
			Point p12 = triangle.p1.add(triangle.p2).div(2);
			Point p13 = triangle.p1.add(triangle.p3).div(2);
			Point p23 = triangle.p2.add(triangle.p3).div(2);

			queue.add(new Triangle(triangle.name + "123", triangle.p1, p12, p123, triangle.border && isBorder(triangle.name.toCharArray(), '3')));
			queue.add(new Triangle(triangle.name + "132", triangle.p1, p123, p13, triangle.border && isBorder(triangle.name.toCharArray(), '2')));
			queue.add(new Triangle(triangle.name + "213", p12, triangle.p2, p123, triangle.border && isBorder(triangle.name.toCharArray(), '3')));
			queue.add(new Triangle(triangle.name + "231", p123, triangle.p2, p23, triangle.border && isBorder(triangle.name.toCharArray(), '1')));
			queue.add(new Triangle(triangle.name + "312", p13, p123, triangle.p3, triangle.border && isBorder(triangle.name.toCharArray(), '2')));
			queue.add(new Triangle(triangle.name + "321", p123, p23, triangle.p3, triangle.border && isBorder(triangle.name.toCharArray(), '1')));
		}

		char[] point = new char[deepth];

		Arrays.fill(point, '1');
		objects.put(new String(point), new Shape(p1));
		Arrays.fill(point, '2');
		objects.put(new String(point), new Shape(p2));
		Arrays.fill(point, '3');
		objects.put(new String(point), new Shape(p3));

		while (! queue.isEmpty())
		{
			triangle = queue.poll();
			objects.put(triangle.name, new Shape(triangle.p1, triangle.p2, triangle.p3));
			if (triangle.border)
			{
				String last = triangle.name.substring(triangle.name.length() - 1, triangle.name.length());
				String key = triangle.name.replace(last, "");
				if (last.equals("1"))
					objects.put(key, new Shape(triangle.p2, triangle.p3));
				else if (last.equals("2"))
					objects.put(key, new Shape(triangle.p1, triangle.p3));
				else if (last.equals("3"))
					objects.put(key, new Shape(triangle.p1, triangle.p2));
			}
		}
	}

	public static void main(String[] args)
	{
		//Test test = new Test();
		double[] gameUtilities = new double[] { 3.0, 2.0, 0.0, 0.0, 0.0, 0.0, 2.0, 3.0 };
		GameTable gameTable = new GameTable(gameUtilities);
		double[] p = new double[]{2.0/5.0, 3.0/5.0, 3.0/5.0, 2.0/5.0};
		gameTable.calculate(p);
		System.out.println("Expected utilities");
		for(double d : gameTable.expU)
			System.out.println(d);
		System.out.println();
		System.out.println("Excess utilities");
		for(double d : gameTable.excU)
			System.out.println(d);
	}
}
