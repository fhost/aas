import java.util.List;

public interface Triangulation
{
	public void dropVertex(List<Vertex> vertices, Vertex drop);
	public void addVertex(List<Vertex> vertices);
        public void initialize(List<Vertex> vertices, int n, int start);
}
